package com.test;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class Test {

    public static void main(String[] args) {
        Timestamp timestamp = new Timestamp(new  Date().getTime());
        ZoneId.getAvailableZoneIds().stream().sorted().forEach(id->{
            ZonedDateTime df = ZonedDateTime.ofInstant(timestamp.toInstant(), ZoneId.of(id));
            LocalDate localDate = df.toLocalDate();
            System.out.println(id+" >>>> "+ localDate);
        });
    }

}
